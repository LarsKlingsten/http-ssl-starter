
mypasswords packages looks this this (remember to replace the const's as they are has been ammended)

package mypasswords

const (

	// ServerSSLCert is the servers SSL certificate, expires in 2045
	// files is found source/golang/src/bitbucket.com/larsklingsetn/ssl-cert/
	serverSSLCert = `-----BEGIN CERTIFICATE-----
	Lars3TCCAsWgAwIBAgIJAN7IEy0s5zT9MA0GCSqGSIb3DQEBCwUAMEsxCzAJBgNV
	BAYTAkRLMRAwDgYDVQQIDAdEZW5tYXJrMREwDwYDVQQHDAhBYXJodXMgQzEXMBUG
	A1UECgwOY2toYW5zZW4uZGsgQ0EwHhcNMTgwMTI5MTQxMzE4WhcNNDUwNjE1MTQx
	MzE4WjCBoDELMAkGA1UEBhMCREsxEDAOBgNVBAgMB0Rlbm1hcmsxETAPBgNVBAcM
	CEFhcmh1cyBDMRIwEAYDVQQKDAlFbmQgUG9pbnQxHTAbBgNVBAsMFFNvZnR3YXJl
	IERldmVsb3BtZW50MR8wHQYJKoZIhvcNAQkBFhBjb2RlQGNraGFuc2VuLmRrMRgw
	FgYDVQQDDA93d3cuY2toYW5zZW4uZGswggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAw
	ggEKAoIBAQCXY7vlCykQQA0EKvQCu8nGJ45PsW+7egY6NHqWtZvcJbZff/4qS/xS
	vG8xfH+LrrBZscLMmufoQa/ldsssssU9aA82d7zDh8tN9cn/PLXEw5i/0hIOBHB
	Q1nRhrMphA3BciXulZut0aB9NlSOpSjBs6c2nAXjEPDY2JoBbAubrY6KMV+bH+pQ
	LuwQYMQO3jkIBnvKjO4HJeyVuxaqYZctL+aa8uaEAo16psrq5PRh/Vpj4UqrR75U
	uJUS0Q4PZmzcjwMf/5TLppTEJdRblRFACol6/nHaQl59yd5gszIjwlcMVqYXj3gm
	ZZt8SZMwvVkAeGSsEM0CetrjyFM66oddddddddddddddddddd1UdIwQYMBaAFDDf
	eBujruTnyfeOE8Eukr/vKm4AMAkGA1UdEwQCMAAwCwYDVR0PBAQDAgTwMDEGA1Ud
	EQQqMCiCEHd3dy5ja2hhbnNlbi5jb22CCWxvY2FsaG9zdIIJMTI3LjAuMC4xMA0G
	CSqGSIb3DQEBCwUAA4IBAQC065+BNks/03pmBpMbcqVwdF705Oy203jU4RUVNvTE
	WQr4KSivBzSOHrtVcWFMFiM/42NQmTnLRWnemReagRipKEgbU1RS8Iex8bM+Hp2y
	WVq8G68N+jaOJw00GLcmSjlhQRxr2YZ2qFy8b1ZjefQU05U6OPoH/IOlYaz/y/Ap
	82YJsj7N0PrfBlKCeBVeJky/1+dFVb+ex7z2q8dWGWY4FWk/ipDfnc3llGGD9TJb
	t79rXu4g49rRPBZYputWGRUH2y5MyGcmSMCJa/Otpg1isjTeLlbVHkYfwiIVw6mi
	PcIh4FHWjuwyNnlRbFMs7ZIUehdTAGAl/yP5rM4zNJSB
	-----END CERTIFICATE-----`

	// ServerSSLKey is the SSL KEY
	serverSSLKey = `-----BEGIN PRIVATE KEY-----
	larsvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCXY7vlCykQQA0E
	KvQCu8nGJ45PsW+7egY6NHqWtZvcJbZff/4qS/xSvG8xfH+LrrBZscLMmufoQa/l
	03OKe2tU9aA82d7zDh8tN9cn/PLXEw5i/0hIOBHBQ1nRhrMphA3BciXulZut0aB9
	NlSOpsjBs6c2nAXjEPDY2JoBbAubrY6KMV+bH+pQLuwQYMQO3jkIBnvKjO4HJeyV
	uxaqYZctL+aa8uaEAo16psrq5PRh/Vpj4UqrR75UuJUS0Q4PZmzcjwMf/5TLppTE
	JdRblRFACol6/nHaQl59yd5gszIjwlcMVqYXj3gmZZt8SZMwvVkAeGSsEM0Cetrj
	yFM66oBVAgMBAAECggEAE4WN3ixBQFIyG356drvGeiHxrP8hfFhw4yKPSE3k6k+G
	o+v6V3Py9qxL3mLCg72kCfztNLso+n818noA5TOjFJKT9iTWJhuFE6ymtwZcPjW7
	3jIzHPT1/Sj1NRqckdbweyiAEbMDHzfv7RWLc9WCWvASuO0HqU8u75eGzHYlTQ6y
	k4u/zJ+PcVOI3//rImCqhVCjo1OLVm5IJsIfHCgG8wYTPwAcHl4h41pOi7Isjy3a
	frlg6mDpnwq7+o0Z4CrQOPqTy2StU46I0QbNyLdSO49oOAxlr7EGCrf+u7IfyblI
	UVHuh7pqET8I+ZOWniJvFgNYssssssssssssssssssssssDGsEQTVxh4nrQqDseJ
	7W6ggnP1px/PxSkp4kmtABcK3m9Q5ZjHUF7/4ef+3DrzaEuStj+69rRozN+xd3Vd
	eT0a2p5OWviMWvXBPlnLpP7gNiTAz6OOi7QfmdNS1I3qHyxqTt8SlnyjQQVQjcJD
	yuw1VTBTUpozeqgJ6+0eatF9GwKBgQDDDsbv+/PvE5EL/nwU8/kn9Mz9Tymq4BBf
	T4+l/73WVyR7BMSBNooRzsfjSpc3AY7VJAwj9o6rShnOB7sssssssssssssssssl
	5J+SGm1Eq/Eer4RGc2Zb25IsUWIIcGTKGQh3+pPaRJyok7H8b4JIKGAgw8YZy0DJ
	EWZeh2X/TwKBgADVboOyQt0GtXTTuQygHW24/mmSePSzPBf75hk+vdoC0u7A9+Im
	OS/Tr9iG7gxlbKPB9gR/0oP6EBo7iReKUbJSJoXnpdFY9F0KOPA80gD42TCh7mrD
	tU7Io3VehPXtiY8so4z9aqp4tGABKEaGjHUw1VIUL6d1pUzdW5JcGmLvAoGAep4Z
	dNcf2xSsssssssssssssssssssssssssssssssssssssssse3V7tzPYg5NU34sjM
	UPc3bHoiip/SVnyDaK3MyOi+jm5N+Eql/QRb2CV2Z+HR9y+oEuqOkwl95Owyj9D4
	v+NnenhTOq3N33CBp7ap5y7qo2BgE4W6ooXfGoUCgYAUH2Shk0sM6QEV/ZaEqV8M
	vbLOcCTluZHTTRNEMgalOhR6NdqnOTyt5776Op5fgi74Y0uA3tC9fx9JFCsf1rp4
	nA2eNqzLxehXrGOvn37YS6yw+rtg4qgd55+LL4he/Q7zcvt54gJSGR7O+QZJrGn8
	v+LqUCTnGmVwFCcdoYpbqw==
	-----END PRIVATE KEY-----`
)

// ServerSSLCert is the servers SSL certificate
func ServerSSLCert() []byte {
	return cleanupString(serverSSLCert)
}

// ServerSSLKey is the servers SSL certificate
func ServerSSLKey() []byte {
	return cleanupString(serverSSLKey)

}

// cleanupString removes unwanted line (CR) return from string literal,
// making them similar to the if the file had been loaded from disk
func cleanupString(strValue string) []byte {
	var bytes []byte
	for _, b := range []byte(strValue) {
		if int(b) != 9 {
			bytes = append(bytes, b)
		}
	}
	return bytes
}

