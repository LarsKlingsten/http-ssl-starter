package main

import (
	"crypto/tls"
	"fmt"
	"log"
	"net/http"
	"time"

	myPasswords "bitbucket.com/larsklingsten/mypasswords"
)

const version = "0.0.3"
const sslport = ":8443"

func helloServer(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "text/plain")
	w.Write([]byte("This is an example server.\nRunning with a self signed SSL certificate\n"))
	w.Write([]byte(fmt.Sprintf("%v\n", time.Now())))
}

func main() {

	// we could load from file like this (but we prefer to use the my Password)
	// cert, key, err := loadCertFromFile()
	// if err!=nil {
	// 	log.Println(err.Error())
	// }

	cert, err := tls.X509KeyPair(myPasswords.ServerSSLCert(), myPasswords.ServerSSLKey())
	if err != nil {
		log.Fatalln("FATAL SSL ERR:" + err.Error())

	}

	tlsConfig := &tls.Config{
		Certificates: []tls.Certificate{cert},
	}
	log.Println(fmt.Sprintf("server started. Connect to https://localhost%s/hello", sslport))

	http.HandleFunc("/hello", helloServer)
	server := http.Server{
		TLSConfig: tlsConfig,
		Addr:      sslport,
	}

	err = server.ListenAndServeTLS("", "")
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
