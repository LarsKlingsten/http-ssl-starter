package main

import (
	"errors"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
)

const certFileName = "server.crt"
const certKeyName = "server.key"
const certpath = "ssl-cert" // place certificates in "ssl-cert" folder in the parent directory

func loadCertFromFile() ([]byte, []byte, error) {

	path, err := getSslDir()

	if err != nil {
		log.Println(err.Error())
	}

	certfile, err := ioutil.ReadFile(path + certFileName)
	if err != nil {
		log.Fatalln("FATAL SSL ERR:" + err.Error())
	}

	keyfile, err := ioutil.ReadFile(path + certKeyName)
	if err != nil {
		log.Fatalln("FATAL SSL ERR:" + err.Error())
	}

	return certfile, keyfile, nil
}

func getSslDir() (string, error) {

	ex, err := os.Getwd()
	if err != nil {
		return "", errors.New("fatal. Could load working directory")
	}
	path := filepath.Dir(ex) + "/" + certpath

	_, err = ioutil.ReadFile(path + "/" + certFileName)
	if err != nil {
		return "", errors.New("Error. Cert '" + certFileName + "' not found.  searched " + path)
	}

	_, err = ioutil.ReadFile(path + "/" + certKeyName)
	if err != nil {
		return "", errors.New("Error. Cert '" + certKeyName + "' not found.  searched " + path)
	}
	return path + "/", nil
}
